package com.example.azzarnoy.cw_cervices;

import android.app.Service;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

public class MyService extends Service {

    @Override
    public void onCreate() {

        SensorManager sm = (SensorManager) getSystemService(SEARCH_SERVICE);

         SensorEventListener listener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {

                    float ax = event.values[0];
                    float ay = event.values[1];
                    float az = event.values[2];
                    double changes = Math.sqrt(ax*ax + ay*ay + az*az);
                    Log.d("Tag os Sensor changed", "Sensor changed" + String.valueOf(changes));

                }



            }



            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }

        };

        sm.registerListener(listener,sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),SensorManager.SENSOR_DELAY_FASTEST);




        super.onCreate();
        Log.d("service", "created");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("service", "destroy");
    }
}
